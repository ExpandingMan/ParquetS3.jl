module ParquetS3

using Parquet2, AWSS3
using AWSS3.FilePathsBase

using Parquet2: Fetcher


"""
    S3Fetcher <: Fetcher

Object which extends the Parquet2.jl `Fetcher` interface for handling
files from S3 and min.io.
"""
struct S3Fetcher <: Fetcher
    path::S3Path
    nbytes::Int
end

Parquet2.default_subset_length(f::S3Fetcher) = 100*1024^2

S3Fetcher(path::S3Path; kw...) = S3Fetcher(path, diskusage(path))
S3Fetcher(str::AbstractString; kw...) = S3Fetcher(S3Path(str))

Parquet2.Fetcher(path::S3Path; kw...) = S3Fetcher(path)

function Parquet2.fetch(f::S3Fetcher, r::AbstractUnitRange)
    @debug("retrieving byte range $r from $path")
    read(f.path, byte_range=r)
end

Base.length(f::S3Fetcher) = f.nbytes


end
