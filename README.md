# ParquetS3

# ***DEPRECATED*** - use Parquet2.jl only, this package not required

[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/ParquetS3.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/ParquetS3.jl/-/pipelines)

This package is an extension to [Parquet2.jl](https://gitlab.com/ExpandingMan/Parquet2.jl) for
loading parquet files from [AWS S3](https://en.wikipedia.org/wiki/Amazon_S3) and
[min.io](https://min.io/).

Please see the [Parquet2.jl documentation](https://expandingman.gitlab.io/Parquet2.jl/) for more
information.

See [AWSS3.jl](https://github.com/JuliaCloud/AWSS3.jl) for generalized Julia tools for dealing with
S3 or [Minio.jl](https://gitlab.com/ExpandingMan/Minio.jl) for a Julia interface to min.io.

