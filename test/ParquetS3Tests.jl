module ParquetS3Tests
__revise_mode__ = :eval

using ParquetS3
using Parquet2
using ReTest, Random, Dates, Parquet2.DecFP
using Parquet2.Tables
using AWSS3, Minio

const ≐ = isequal

include(joinpath(dirname(pathof(Parquet2)),"..","test","gentables.jl"))

const BUCKET_DIR = joinpath(dirname(pathof(Parquet2)),"..","test")

const MINIO_PORT = 9005

const MINIO_SERVER = Minio.Server(BUCKET_DIR; address="localhost:$MINIO_PORT")

const MINIO_CONFIG = MinioConfig("http://localhost:$MINIO_PORT"; username="minioadmin", password="minioadmin")

AWSS3.AWS.global_aws_config(MINIO_CONFIG)

#TODO: will need to start with a different directory for writing
# (not guaranteed we can always write to this one)


live() = Minio.run(MINIO_SERVER, wait=false)

function kill()
    try
        Minio.kill(MINIO_SERVER)
        @info("killed Minio test server")
    catch e
        @warn("errored killing minio server", exception=(e, catch_backtrace()))
    end
end


function _may_be_dir(s::AbstractString)
    for p ∈ ("hive", "simple_spark")
        startswith(s, p) && return true
    end
    false
end

function testfilename(s::Symbol)
    s = string(s)
    # need to ensure directories include trailing slach for S3
    sl = _may_be_dir(s) ? "/" : ""
    joinpath("s3://data", s*".parq"*sl)
end

testloads3(file::Symbol) = Parquet2.Dataset(S3Path(testfilename(file)))


# this is deliberately broken into multiple tests to be more manageable
function table_compare(df1, df2)
    cols1, cols2 = Tables.Columns.(Tables.columns.((df1, df2)))
    @test collect(propertynames(cols1)) == collect(propertynames(cols2))
    for ((k1,v1), (k2,v2)) ∈ zip(pairs(cols1), pairs(cols2))
        @test string(k1) == string(k2)
        @test v1 ≐ v2
    end
end


@testset "reference" begin
    @testset "std" begin
        tbl = standard_test_table()
        @testset "fastparquet" begin
            table_compare(tbl, testloads3(:std_fastparquet))
        end
        @testset "pyarrow" begin
            table_compare(tbl, testloads3(:std_pyarrow))
        end
    end

    @testset "rand" begin
        tbl = random_test_table()
        @testset "fastparquet" begin
            table_compare(tbl, testloads3(:rand_fastparquet))
        end
        @testset "pyarrow" begin
            table_compare(tbl, testloads3(:rand_pyarrow))
        end
    end

    @testset "compressed" begin
        tbl = standard_test_table()
        @testset "fastparquet" begin
            table_compare(tbl, testloads3(:compressed_fastparquet))
        end
        tbl = random_test_table()  # try to get a bit more variety in tests
        @testset "pyarrow" begin
            table_compare(tbl, testloads3(:compressed_pyarrow))
        end
    end

    @testset "extratypes_fastparquet" begin
        ds = testloads3(:extra_types_fastparquet)
        tbl = Tables.Columns(ds)
        dct = make_json_dicts(1)[1]
        @test all(==(dct), tbl.jsons)
        @test tbl.jvm_timestamps == [DateTime(2022,3,8) + Day(i) + Minute(1) for i ∈ 0:4]
        # note that fastparquet outputs these not as strings but arrays
        vs = [b"a\0\0\0\0", b"ab\0\0\0", b"abc\0\0", b"abcd\0", b"abcde"]
        @test tbl.fixed_strings == vs
    end

    @testset "extratypes_pyarrow" begin
        ds = testloads3(:extra_types_pyarrow)
        tbl = Tables.Columns(ds)
        @test all(x -> x isa DecFP.DecimalFloatingPoint, tbl.decimals)
        @test tbl.decimals == Dec64[1.0, 2.1, 3.2, 4.3, 5.4]
        @test tbl.dates == [Date(1990,1,i) for i ∈ 1:5]
        @test tbl.times_of_day == [Time(i) for i ∈ 1:5]
    end

    @testset "hive_fastparquet" begin
        ds = testloads3(:hive_fastparquet)
        tbl = Tables.Columns(ds)
        @test tbl.A == string.([1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3])
        @test tbl.B == [fill("alpha", 8); fill("beta", 4)]
        @test tbl.data1 == 1.0:12.0
        @test tbl.data2 ≐ [1:11; missing]
    end

    @testset "simple_spark" begin
        ds = testloads3(:simple_spark)
        append!.((ds,), Parquet2.filelist(ds))
        tbl = Tables.Columns(ds)
        @test tbl.A == ["test1", "test2"]
        @test tbl.id == [1, 2]
        @test tbl.Date == [Date(2020,1,1), Date(2020,1,2)]
    end
end


end
